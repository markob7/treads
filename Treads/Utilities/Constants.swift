//
//  Constants.swift
//  Treads
//
//  Created by Marko Bizjak on 31/10/2017.
//  Copyright © 2017 Marko Bizjak. All rights reserved.
//

import Foundation

let REALM_QUEUE = DispatchQueue(label: "realmQueue")
