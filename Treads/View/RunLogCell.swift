//
//  RunLogCell.swift
//  Treads
//
//  Created by Marko Bizjak on 29/10/2017.
//  Copyright © 2017 Marko Bizjak. All rights reserved.
//

import UIKit

class RunLogCell: UITableViewCell {
    
    @IBOutlet weak var runDurationLbl: UILabel!
    @IBOutlet weak var totalDistanceLbl: UILabel!
    @IBOutlet weak var averagePaceLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func configureCell(run: Run) {
        runDurationLbl.text = run.duration.formatTimeDurationToString()
        totalDistanceLbl.text = "\(run.distance.metersToKilometers(places: 2)) km"
        averagePaceLbl.text = run.pace.formatTimeDurationToString()
        dateLbl.text = run.date.getDateString()
    }
    
}
