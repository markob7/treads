//
//  SecondViewController.swift
//  Treads
//
//  Created by Marko Bizjak on 28/10/2017.
//  Copyright © 2017 Marko Bizjak. All rights reserved.
//

import UIKit

class RunLogVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var runLogTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        runLogTableView.delegate = self
        runLogTableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Run.getAllRuns()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = runLogTableView.dequeueReusableCell(withIdentifier: "RunLogCell", for: indexPath) as? RunLogCell {
            if let runs = Run.getAllRuns() {
                cell.configureCell(run: runs[indexPath.row] )
                return cell
            } else {
                return RunLogCell()
            }
        }
        
        return RunLogCell()
    }

}

